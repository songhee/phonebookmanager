package phoneBookVer04;

public class PhoneInfo {
	String name;
	String phoneNumber;
	
	public PhoneInfo(String name, String num) {
		
		this.name=name;
		this.phoneNumber=num;
		
	}
	
	public void showPhoneInfo() {
		System.out.println("name : "+name);
		System.out.println("phone : "+phoneNumber);
	}

}

class PhoneUniveInfo extends PhoneInfo
{
	String major;
	int year;
	
	public PhoneUniveInfo(String name, String num, String major, int year) {
		super(name, num);
		this.major = major;
		this.year = year;
	}
	
	public void showPhoneInfo() {
		super.showPhoneInfo();
		System.out.println("major : "+major);
		System.out.println("year : "+year);
	}
	
}

class PhoneCompanyInfo extends PhoneInfo
{

	String company;
	public PhoneCompanyInfo(String name, String num, String company) {
		super(name, num);
		this.company= company;
		
	}
	
	public void showPhoneInfo() {
		super.showPhoneInfo();
		System.out.println("company : "+company);
	}
	
}


